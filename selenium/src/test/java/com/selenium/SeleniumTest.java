package com.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SeleniumTest {

    private WebDriver driver;

    @Autowired
    private Environment env;

    @BeforeClass
    public static void start() {
       // final String webDriverPath = "replace with the path of the web driver";
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Administrator\\POC\\chromedriver\\chromedriver.exe");
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws InterruptedException {
        String environmentIp = env.getProperty("ENVIRONMENT_IP");
        System.out.println("Environment Ip "+ environmentIp);
        String appUrl = "http://"+ environmentIp + ":8080/SpringBootMongo/employee";
        System.out.println("Application URL "+ appUrl );
        driver.get(appUrl);
        driver.manage().window().maximize();
        Thread.sleep(5000);

        driver.findElement(By.xpath("/html/body/a[1][contains(.,'Add Employee')]")).click();
        Thread.sleep(5000);

        // final WebElement dropdown = driver.findElement(By.linkText("Dropdown Test"));
        // dropdown.click();

        // Thread.sleep(1500);

        // final WebElement choice2 = driver.findElement(By.linkText("Test 2"));
        // choice2.click();

        // Thread.sleep(1500);

        // final String currentUrl = driver.getCurrentUrl();
        // assertThat(currentUrl).endsWith("/test/2");
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
