$testStatus = Select-String -Path selenium\target\surefire-reports\SeleniumTest.xml -Pattern 'failures="0"'
if ($testStatus -eq $null) {
    Write-Host 'Selenium test failed'
    exit 1} 
else {
    Write-Host 'Selenium test succeeded'
    exit 0}

