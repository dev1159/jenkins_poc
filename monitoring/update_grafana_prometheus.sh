#!/bin/bash

JENKINS_MASTER_IP=$1
SONARIP=$2
EKS_PROMETHEUS=$3

echo  "
global:
  scrape_interval:     15s
  evaluation_interval: 15s

rule_files:
  # - "first.rules"
  # - "second.rules"

scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']
  - job_name: 'node_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9100']
  - job_name: 'jenkins_node'
    scrape_interval: 5s
    metrics_path: '/prometheus'
    static_configs:
      - targets: ['$JENKINS_MASTER_IP:8080']
  - job_name: 'sonarqube'
    metrics_path: '/api/prometheus/metrics'
    scrape_interval: 5s
    static_configs:
      - targets: ['$SONARIP:9000']
    basic_auth:
      username: 'admin'
      password: 'sonar'
" > /etc/prometheus/prometheus.yml && ls /etc/prometheus

sudo systemctl restart prometheus

sudo touch /etc/grafana/provisioning/datasources/datasource.yaml && sudo chown root:grafana /etc/grafana/provisioning/datasources/datasource.yaml && sudo chmod 777 /etc/grafana/provisioning/datasources/datasource.yaml && echo "
apiVersion: 1
datasources:
 - name: eks_prometheus
   type: prometheus
   access: proxy
   url: http://$EKS_PROMETHEUS:9090
   isDefault: false
   version: 1
   editable: false

" > /etc/grafana/provisioning/datasources/datasource.yaml && ls  /etc/grafana/provisioning/datasources

sudo /etc/init.d/grafana-server restart
