package com.spring.boot.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.spring.boot.mongo.entity.Employee;

@Repository
public interface EmployeeMongoRespository extends MongoRepository<Employee, String> {

	Long deleteByEmpId(Long empId);
}
