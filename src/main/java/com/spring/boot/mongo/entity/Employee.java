package com.spring.boot.mongo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employee")
public class Employee{
	@Id
	private Long empId;

	private String designation;

	private String name;
	
	private Double salary;
		
	public Employee() {	
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(final Long empNo) {
		this.empId = empNo;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(final String position) {
		this.designation = position;
	}

	public String getName() {
		return name;
	}

	public void setName(final String alias) {
		this.name = alias;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(final Double income) {
		this.salary = income;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", designation=" + designation + ", name=" + name + ", salary=" + salary
				+ "]";
	}

	public Employee(final Long empNo, final String position, final String alias, final Double income) {
		super();
		this.empId = empNo;
		this.designation = position;
		this.name = alias;
		this.salary = income;
	}			
		
}
