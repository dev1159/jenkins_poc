package com.spring.boot.mongo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.spring.boot.mongo.controller.EmployeeController;
import com.spring.boot.mongo.entity.EmployeeDTO;
import com.spring.boot.mongo.entity.Employee;
import com.spring.boot.mongo.repository.EmployeeMongoRespository;

@ExtendWith(MockitoExtension.class)
public class EmployeeControllerUnitTest
{ 
	@InjectMocks
	EmployeeController employeeController;

    @Mock
    EmployeeMongoRespository repository;

    @Test
	public void testAddEmployee() 
	{
		final MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		final Employee employee = new Employee();
		employee.setEmpId(1L);
		when(repository.save(any(Employee.class))).thenReturn(employee);

		final EmployeeDTO employeeToAdd = new EmployeeDTO(1001L, "Accountant", "Gupta", 35000d);
		final ResponseEntity<Void> responseEntity = employeeController.addEmployee(employeeToAdd);

		assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
		// assertThat(responseEntity.getHeaders().getLocation().getPath()).isEqualTo("/1");
	}

	@Test
	public void testFetchEmployees() {
		// given
		final Employee employee1 = new Employee(1001L, "Accountant", "Gupta", 35000d);
		final Employee employee2 = new Employee(1002L, "Manager", "Lokesh", 65000d);
		final List<Employee> list = new ArrayList<Employee>();
		list.addAll(Arrays.asList(employee1, employee2));

		Mockito.when(repository.findAll()).thenReturn(list);

		// when
		final ResponseEntity<List<Employee>> responseEntity = employeeController.fetchEmployees();

		// then
		assertThat(responseEntity.getBody().size()).isEqualTo(2);
		
		assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);

	}

}
