#!groovy

import groovy.json.JsonSlurper
import java.net.URL


pipeline {
    agent any
       
    options {
        timeout(time: 1, unit: 'DAYS')
        disableConcurrentBuilds()
        gitLabConnection('Gitlab')   
    }
  
  
  
    stages {
        stage("Init") {          
            steps { 
                script { env.FAILED_STAGE = env.STAGE_NAME 
                 }
                initialize()                                
           }
        }
        
        stage("Build App") { 
            when {
                expression {env.GIT_BRANCH != 'origin/master'}
            }          
            steps {
                script { 
                    env.FAILED_STAGE=env.STAGE_NAME
                    buildApp() 
                 } 
             }
        }

        
        stage('Code review') {
            when {
                expression {env.GIT_BRANCH != 'origin/master'}
            }
            parallel {
                
            stage("SonarQube Analysis") { 
                environment { SONARQUBETOKEN = credentials('sonarqube') }
                steps {  script {
                    env.FAILED_STAGE=env.STAGE_NAME 
                    runQualityAnalysis() }}
            }
            stage("PMD and CheckStyle Scan") { 
                steps {  script { 
                    env.FAILED_STAGE=env.STAGE_NAME
                    runPmdCheckStyleTest() }}
            }
        }
        }
        

        stage("Build and Register Image") {
            when {
                expression {env.GIT_BRANCH != 'origin/master'}
            }
            steps { script { 
                env.FAILED_STAGE=env.STAGE_NAME
                buildAndRegisterDockerImage() } }
        } 

        stage('Infra DEV Setup') {
            when {
                expression {env.GIT_BRANCH != 'origin/master'}
            }
            steps {
            script {
          env.FAILED_STAGE=env.STAGE_NAME       
          build job: 'Terraform',parameters: [string(name: 'action', value: 'create'), string(name: 'cluster', value: 'ggm-eks') ], propagate: true, wait: true
          sleep time: 30, unit: 'SECONDS'
          env.ELB_ADD = findLoadBalancerinLinux()
          env.EKS_PROMETHEUS = findPrometheusLoadBalancerinLinux()
          
            } }
        }

        stage("Metrics Monitoring : DEV") {
            when {
                expression {env.GIT_BRANCH != 'origin/master'}
            }
            agent { label 'Prometheus_and_grafana'}
            steps {
                script { env.FAILED_STAGE=env.STAGE_NAME }
                prom_and_grafana()
                
            }
        }

        stage("QA Automation"){

            when {
                expression {env.GIT_BRANCH != 'origin/master'}
            }

            parallel {
            stage("Selenium Test App in Dev") {
            agent { label 'windows_selenium'}
            steps { script { 
                env.FAILED_STAGE=env.STAGE_NAME
                runSeleniumTest(env.ENVIRONMENT) }}
             }

            stage("Jmeter Test App in Dev") {
            steps { script { 
                env.FAILED_STAGE=env.STAGE_NAME
                runJmeterTest(env.ENVIRONMENT)         } }
            }

            stage("ZAP test") {
            agent { label 'windows_selenium'}
            steps { script { 
                env.FAILED_STAGE=env.STAGE_NAME
                runZAPTest(env.ENVIRONMENT) }  }
            } 
           }

           
	}      

        stage('Destroy DEV Infra Setup') {
            
          when {
            expression {env.GIT_BRANCH != 'origin/master'}
        }

       steps {
       script {
           env.FAILED_STAGE=env.STAGE_NAME 
          build job: 'Terraform',parameters: [string(name: 'action', value: 'destroy'), string(name: 'cluster', value: 'ggm-eks') ], propagate: true, wait: true
       
          }
        }
        }
//===========================================================================
//prod deploy================================================================
//===========================================================================
        stage("Proceed to prod?") {
           
            
        when {
                expression {env.GIT_BRANCH == 'origin/master'}
        }
        steps { proceedTo('prod') }
        }


        stage("Deploy Image to Prod") {
            
            
        when {
                expression {env.GIT_BRANCH == 'origin/master'}
            }

        steps { 
                
        script {
          env.FAILED_STAGE=env.STAGE_NAME 
          build job: 'Terraform',parameters: [string(name: 'action', value: 'create'), string(name: 'cluster', value: 'prod-ggm-eks') ], propagate: true, wait: true
          env.ELB_ADD = findLoadBalancerinLinux()
          env.EKS_PROMETHEUS = findPrometheusLoadBalancerinLinux()       
             
                }
        }  
    }
    stage("Metrics Monitoring : PROD") {
            agent { label 'Prometheus_and_grafana'}
            when {
                expression {env.GIT_BRANCH == 'origin/master'}
            }
            steps {
                script { env.FAILED_STAGE=env.STAGE_NAME }
                prom_and_grafana()
                
            }
        }

    }
post
{
    success {
            addGitLabMRComment(comment: 'The pipeline was run on Jenkins')
            script {
            if (env.GIT_BRANCH != 'origin/master') {                                          
                notifyStarted()
            } 
            else {  
                notifyProd()                                 
            } 
            }
            cleanWs()
    }
    failure {
            notifyfailed()
            cleanws()
    }
    aborted {
            notifyAborted()
            cleanWs()
    }
   
}
}

// ================================================================================================
// Initialization steps
// ================================================================================================

def initialize() {
   
    env.AWS_REGION = "us-east-2"
    env.REGISTRY_URL = "785131266845.dkr.ecr.us-east-2.amazonaws.com"
    env.MAX_ENVIRONMENTNAME_LENGTH = 32
    env.JENKINS_MASTER_IP = getJenkinsMaster()
    env.SONARIP = findSonarQubeIp()
    env.DATE = currentDate() 
    env.GRAFANAIP = findGrafanaIp()
    setEnvironment()
    env.IMAGE_NAME = "ggm_repo:" + "latest"
    showEnvironmentVariables()
}

def setEnvironment() {
    def branchName = env.gitlabBranch.toLowerCase()
    def environment = 'dev'
    echo "branchName = ${branchName}"
    if (branchName == "") {
        showEnvironmentVariables()
        throw "gitlabBranch is not an environment variable or is empty"
    } else if (branchName != "master") {
        
        if (branchName.contains("/")) {
//ignore branch type
            branchName = branchName.split("/")[1]
        }
//echo "remove '-' characters'"
        branchName = branchName.replace("-", "")           
        branchName = branchName.take(env.MAX_ENVIRONMENTNAME_LENGTH as Integer)
        environment += "-" + branchName
    }
    echo "Using environment: ${environment}"
    env.ENVIRONMENT = environment
}

def showEnvironmentVariables() {
    sh 'env | sort > env.txt'
    sh 'cat env.txt'
}

def buildApp() {
    sh "mvn clean install -DskipTests"
    archiveArtifacts 'target/*.war'

    sh """ aws s3 cp target/*.war s3://jenkinsartifacts/Artifact/app_${env.DATE}.war """
    sh "mvn test"

    sh """ aws s3 cp target/surefire-reports/*.xml s3://jenkinsartifacts/JUnit/report_${env.DATE}.xml """

    step([$class: 'JUnitResultArchiver', testResults: 'target/surefire-reports/TEST-*.xml'])

    jacoco()
    sh """ aws s3 cp target/site/jacoco/*.xml s3://jenkinsartifacts/Code_coverage/report_${env.DATE}.xml """

}

//================================================================================================
// SAST
//================================================================================================

//====== SonarQube==============================================================================

def runQualityAnalysis() {
    def sonarReportDir = "target/sonar"
    def sonarqubeIP = findSonarQubeIp()
    
    withSonarQubeEnv('sonarqube') {
                    
    sh "mvn sonar:sonar -Dsonar.host.url=http://${sonarqubeIP}:9000 -Dsonar.login=${SONARQUBETOKEN} "
    
    sh 'ls -al $sonarReportDir'
    loadProperties()
    }
   
    
//====Buildbreaker==============================================================================

    timeout(time: 1, unit: 'MINUTES') { 
           def qg = waitForQualityGate() 
           if (qg.status != 'OK') {
             error "Pipeline aborted due to quality gate failure: ${qg.status}"
           }
        }
        
}

 def loadProperties(){
    def getURL = readProperties file: 'target/sonar/report-task.txt'
    env.sonarqubeURL = "${getURL['dashboardUrl']}"
    echo "${env.sonarqubeURL}"
    
    }
 
//======Finding SonarQube IP================================================

def findSonarQubeIp() {
    def ip = ""
       ip = sh(returnStdout: true,
            script: """aws ec2 describe-instances \
                --filters "Name=instance-state-name,Values=running" \
                "Name=tag:Name,Values=SonarQube" \
                --query "Reservations[].Instances[].{Ip:PublicIpAddress}" \
                --output text --region ${env.AWS_REGION} | tr -d '\n'
"""
        )
    echo "ip=[${ip}]"
    return ip
}

//==============================================================================================
//=======PMD and Checkstyle (SAST)==============================================================


def runPmdCheckStyleTest() {
    sh "mvn pmd:pmd checkstyle:checkstyle"
    recordIssues(tools: [checkStyle(), pmdParser()])
}


def buildAndRegisterDockerImage() {
    def buildResult
    echo "Connect to registry at ${env.REGISTRY_URL}" 
    sh "aws ecr get-login-password --region ${env.AWS_REGION} | docker login --username AWS --password-stdin ${env.REGISTRY_URL}"
    echo "Build ${env.REGISTRY_URL}/${env.IMAGE_NAME}"
    sh "docker build -t ${env.REGISTRY_URL}/${env.IMAGE_NAME} ."
    echo "Register ${env.IMAGE_NAME} at ${env.REGISTRY_URL}"
    sh "docker push ${env.REGISTRY_URL}/${env.IMAGE_NAME}"
    echo "Disconnect from registry at ${env.REGISTRY_URL}"
    sh "docker logout ${env.REGISTRY_URL}"
}



//========Continuous Testing ====================================================


//========Selenium===============================================================

def runSeleniumTest(environment) {
    
    env.ENVIRONMENT_IP = env.ELB_ADD

//===PREBUILD STEP =========================================================


    bat "START C:\\Users\\Administrator\\POC\\chromedriver\\chromedriver.exe"
    bat "START javaw -jar C:\\Users\\Administrator\\Downloads\\selenium-server-standalone-3.141.59.jar"


//===BUILD STEP=============================================================  

    bat "mvn test -f ./selenium/pom.xml"
    bat """
    cd selenium\\target\\surefire-reports & 
    del SeleniumTest.xml &
    rename TEST-com.selenium.SeleniumTest.xml SeleniumTest.xml 
    """
    bat "dir selenium\\target\\surefire-reports"
    bat """ aws s3 cp selenium/target/surefire-reports/SeleniumTest.xml s3://jenkinsartifacts/Selenium/report_${env.DATE}.xml """


//===BUILD BREAKER===========================================================

    powershell returnStatus: true, script: ".\\scripts\\seleniumbreaker.ps1"
     
    
}

//============Jmeter============================================================


def runJmeterTest(environment) {
    
     
    sh "mvn jmeter:jmeter -DdomainIp=${env.ELB_ADD}"

    perfReport 'target/jmeter/results/*.csv'
    sh """ aws s3 cp target/jmeter/results/*.csv s3://jenkinsartifacts/Jmeter/report_${env.DATE}.csv """

//====Build breaker=================================================

    sh """
        if grep -q "0.00%" "target/jmeter/logs/employee.jmx.log"; then
            exit 0
        else
            exit 1
        fi

    """
    
}

//======ZAP===================================================================

def runZAPTest(environment) {
    
    def env_Ip = env.ELB_ADD
    def address = "http://${env_Ip}:8080/SpringBootMongo/employee"

    bat  """java -jar "C:\\Program Files\\OWASP\\Zed Attack Proxy\\zap-2.10.0.jar" -cmd -quickurl ${address} -quickprogress > C:\\Users\\Administrator\\jenkins\\ZAP\\result.xml """

    bat """ aws s3 cp C:\\Users\\Administrator\\jenkins\\ZAP\\result.xml s3://jenkinsartifacts/Zap/report_${env.DATE}.xml """

//===buildbreaker =============================================================

    powershell returnStatus: true, script: ".\\scripts\\zap.ps1"

}





//=======Deploy to PROD=============================================================

def proceedTo(environment) {
    def description = "Choose 'yes' if you want to deploy to this build to " + 
        "the ${environment} environment"
    def proceed = 'no'
    timeout(time: 4, unit: 'HOURS') {
        proceed = input message: "Do you want to deploy the changes to ${environment}?",
            parameters: [choice(name: "Deploy to ${environment}", choices: "no\nyes",
                description: description)]
        if (proceed == 'no') {
            error("User stopped pipeline execution")
        }
    }
}

//=========================================================================================
//============Monitoring  =================================================================
//=========================================================================================


def prom_and_grafana() {

    sh """ chmod +x monitoring/update_grafana_prometheus.sh && bash monitoring/update_grafana_prometheus.sh ${env.JENKINS_MASTER_IP} ${env.SONARIP} ${env.EKS_PROMETHEUS} """

}


//==========Notification===================================================================



def notifyStarted() {
    sh "mkdir -p /var/lib/jenkins/email-templates && cp email_notif/email_success.jelly /var/lib/jenkins/email-templates/email_success.jelly && ls /var/lib/jenkins/email-templates"
    
    env.ForEmailPlugin = env.WORKSPACE 
    
    emailext mimeType: 'text/html',
        body: ''' ${JELLY_SCRIPT,template="email_success"} ''',
        subject: currentBuild.currentResult + " : " + env.JOB_NAME,
        to:  '${DEFAULT_RECIPIENTS}'

}


def notifyfailed() {
    sh "mkdir -p /var/lib/jenkins/email-templates && cp email_notif/email_failure.jelly /var/lib/jenkins/email-templates/email_failure.jelly && ls /var/lib/jenkins/email-templates"
    
    env.ForEmailPlugin = env.WORKSPACE 
    
    emailext mimeType: 'text/html',
        attachLog: true,
        body: ''' ${JELLY_SCRIPT,template="email_failure"} ''',
        subject: currentBuild.currentResult + " : " + env.JOB_NAME,
        to:  '${DEFAULT_RECIPIENTS}'

}

def notifyAborted() {
    sh "mkdir -p /var/lib/jenkins/email-templates && cp email_notif/email_unstable.jelly /var/lib/jenkins/email-templates/email_unstable.jelly && ls /var/lib/jenkins/email-templates"
    
    env.ForEmailPlugin = env.WORKSPACE 
    
    emailext mimeType: 'text/html',
        attachLog: true,
        body: ''' ${JELLY_SCRIPT,template="email_unstable"} ''',
        subject: currentBuild.currentResult + " : " + env.JOB_NAME,
        to:  '${DEFAULT_RECIPIENTS}'

}

def notifyProd() {
    sh "mkdir -p /var/lib/jenkins/email-templates && cp email_notif/email_prod.jelly /var/lib/jenkins/email-templates/email_prod.jelly && ls /var/lib/jenkins/email-templates"
    
    env.ForEmailPlugin = env.WORKSPACE 
    
    emailext mimeType: 'text/html',
        body: ''' ${JELLY_SCRIPT,template="email_prod"} ''',
        subject: currentBuild.currentResult + " : " + env.JOB_NAME,
        to:  '${DEFAULT_RECIPIENTS}'

}


//========DATE===============================================================

def currentDate() {
    def currentDate = ""
    currentDate = sh(returnStdout: true, 
                  script: """ date +"%Y-%m-%d-%T" | tr -d '\n' """ )
    echo "currentDate=[${currentDate}]"
    return currentDate
}


//========Jenkins Master=======================================================

def getJenkinsMaster() {
    return env.BUILD_URL.split('/')[2].split(':')[0]
}


//=======Finding IP address=================================================


def findLoadBalancerinLinux(){
    def elb_address = ""
    elb_address = sh(returnStdout: true, 
                  script: """ kubectl get svc -n employee web-service -o json |  jq .status.loadBalancer.ingress[0].hostname | tr -d '"' | tr -d '\n' """ )
    echo "elb_address=[${elb_address}]"
    return elb_address
}

def findPrometheusLoadBalancerinLinux(){
    def prom_elb_address = ""
    prom_elb_address = sh(returnStdout: true, 
                  script: """ kubectl get svc -n prometheus prometheus-server -o json |  jq .status.loadBalancer.ingress[0].hostname | tr -d '"' | tr -d '\n' """ )
    echo "prom_elb_address=[${prom_elb_address}]"
    return prom_elb_address
}





def findGrafanaIp() {
    def ip = ""
       ip = sh(returnStdout: true,
            script: """aws ec2 describe-instances \
                --filters "Name=instance-state-name,Values=running" \
                "Name=tag:Name,Values=Prometheus_and_Grafana" \
                --query "Reservations[].Instances[].{Ip:PublicIpAddress}" \
                --output text --region ${env.AWS_REGION} | tr -d '\n'
"""
        )
    echo "ip=[${ip}]"
    return ip
}


