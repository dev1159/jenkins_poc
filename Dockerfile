FROM tomcat:8.0.51-jre8-alpine
RUN rm -rf /usr/local/tomcat/webapps/*
COPY target/SpringBootMongo.war /usr/local/tomcat/webapps/SpringBootMongo.war
CMD ["catalina.sh","run"]
